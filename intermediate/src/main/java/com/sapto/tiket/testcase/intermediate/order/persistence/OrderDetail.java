package com.sapto.tiket.testcase.intermediate.order.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.sapto.tiket.testcase.intermediate.product.persistence.Product;

@Entity
public class OrderDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer orderDetailID;

	@Column
	private Integer quantity;
	@Column
	private Double unitPrice;
	@Column
	private Integer discount;

	@ManyToOne
	private Product product;

	@ManyToOne
	private Orders orders;
	
	public OrderDetail() {
		// TODO Auto-generated constructor stub
	}
	

	public OrderDetail(Integer orderDetailID, Integer quantity, Double unitPrice, Integer discount, Product product,
			Orders orders) {
		super();
		this.orderDetailID = orderDetailID;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.discount = discount;
		this.product = product;
		this.orders = orders;
	}

	public Integer getOrderDetailID() {
		return orderDetailID;
	}

	public void setOrderDetailID(Integer orderDetailID) {
		this.orderDetailID = orderDetailID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Orders getOrders() {
		return orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

}
