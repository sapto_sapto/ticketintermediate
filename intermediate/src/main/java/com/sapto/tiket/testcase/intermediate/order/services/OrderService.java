package com.sapto.tiket.testcase.intermediate.order.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.sapto.tiket.testcase.intermediate.order.persistence.Orders;

public interface OrderService {
	
	Page<Orders> findAllOrder(PageRequest pageReq);

}
