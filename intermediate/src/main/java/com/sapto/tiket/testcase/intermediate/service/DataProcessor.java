package com.sapto.tiket.testcase.intermediate.service;

public interface DataProcessor {

	public String processEmployee();

	public String processProduct();

	public String processCustomer();
	
	public String processShippingMethod();
	
	public String processOrders();
	
	public String processOderDetail();

}
