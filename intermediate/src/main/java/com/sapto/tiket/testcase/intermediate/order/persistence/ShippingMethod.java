package com.sapto.tiket.testcase.intermediate.order.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ShippingMethod implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer shippingMethodID;

	@Column(length = 20)
	private String shippingMethodName;
	
	public ShippingMethod() {
		// TODO Auto-generated constructor stub
	}
	
	public ShippingMethod(Integer id, String shippingMethodName) {
		this.shippingMethodID =id;
		this.shippingMethodName=shippingMethodName;
	}

	public Integer getShippingMethodID() {
		return shippingMethodID;
	}

	public void setShippingMethodID(Integer shippingMethodID) {
		this.shippingMethodID = shippingMethodID;
	}

	public String getShippingMethodName() {
		return shippingMethodName;
	}

	public void setShippingMethodName(String shippingMethodName) {
		this.shippingMethodName = shippingMethodName;
	}

}
