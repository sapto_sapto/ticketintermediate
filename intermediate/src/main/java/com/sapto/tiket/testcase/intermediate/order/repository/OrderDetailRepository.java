package com.sapto.tiket.testcase.intermediate.order.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.sapto.tiket.testcase.intermediate.order.persistence.OrderDetail;

public interface OrderDetailRepository extends PagingAndSortingRepository<OrderDetail, Integer>{

}
