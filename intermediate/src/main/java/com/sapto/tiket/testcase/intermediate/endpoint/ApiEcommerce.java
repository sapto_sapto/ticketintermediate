package com.sapto.tiket.testcase.intermediate.endpoint;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sapto.tiket.testcase.intermediate.order.persistence.Orders;
import com.sapto.tiket.testcase.intermediate.order.services.OrderService;
import com.sapto.tiket.testcase.intermediate.service.DataProcessor;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
@ApiOperation(value = " API for Ecommerce")
public class ApiEcommerce {

	@Autowired
	private DataProcessor csvProcessor;

	@Qualifier("orderServiceRDBMS")
	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/import/employee", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Employee from CSV")
	public ResponseEntity<String> importEmployee(HttpServletRequest request) {

		String response = csvProcessor.processEmployee();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/import/product", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Employee from CSV")
	public ResponseEntity<String> importProduct(HttpServletRequest request) {

		String response = csvProcessor.processProduct();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/import/customer", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Customer from CSV")
	public ResponseEntity<String> importCustomer(HttpServletRequest request) {

		String response = csvProcessor.processCustomer();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/import/shippingMethod", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Shipping Method from CSV")
	public ResponseEntity<String> importShippingMethod(HttpServletRequest request) {

		String response = csvProcessor.processShippingMethod();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/import/order", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Order from CSV")
	public ResponseEntity<String> importOrder(HttpServletRequest request) {

		String response = csvProcessor.processOrders();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/import/orderDetail", method = RequestMethod.GET)
	@ApiOperation(value = "Import Data Order Detail from CSV")
	public ResponseEntity<String> importOrderDetail(HttpServletRequest request) {

		String response = csvProcessor.processOderDetail();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/findAll/order/{page}/{size}", method = RequestMethod.GET)
	@ApiOperation(value = "Find Data Order by Page (a,b)")
	public Page<Orders> findAllOrder(HttpServletRequest request, @PathVariable("page") Integer page,
			@PathVariable("size") Integer size) {

		Page<Orders> orders = orderService.findAllOrder(PageRequest.of(page, size));

		return orders;
	}

}
