package com.sapto.tiket.testcase.intermediate.customer.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.sapto.tiket.testcase.intermediate.customer.persistence.Customer;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer> {

}
