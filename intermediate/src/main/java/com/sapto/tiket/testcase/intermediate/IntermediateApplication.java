package com.sapto.tiket.testcase.intermediate;



import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan(basePackages = "com.sapto.tiket.testcase.intermediate")
public class IntermediateApplication{

	@Autowired
	DataSource dataSource;

	public static void main(String[] args) {
		SpringApplication.run(IntermediateApplication.class, args);
	}
}
