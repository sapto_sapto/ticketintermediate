package com.sapto.tiket.testcase.intermediate.employee.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.sapto.tiket.testcase.intermediate.persistence.BaseUser;
import com.sapto.tiket.testcase.intermediate.persistence.IBaseUser;

@Entity
public class Employee extends BaseUser implements IBaseUser, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer employeeID;

	@Column(length=50)
	private String title;
	@Column(length=50)
	private String workPhone;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	
	public Employee(Integer employeeID, String firstName, String lastName, String title, String workPhone) {
		this.employeeID = employeeID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.workPhone = workPhone;
	}

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	@Column(length=50)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(length=50)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

}
