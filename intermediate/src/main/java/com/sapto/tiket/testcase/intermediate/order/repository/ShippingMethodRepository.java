package com.sapto.tiket.testcase.intermediate.order.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.sapto.tiket.testcase.intermediate.order.persistence.ShippingMethod;

public interface ShippingMethodRepository extends PagingAndSortingRepository<ShippingMethod, Integer>{

}
