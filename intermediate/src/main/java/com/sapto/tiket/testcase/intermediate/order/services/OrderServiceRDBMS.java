package com.sapto.tiket.testcase.intermediate.order.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.sapto.tiket.testcase.intermediate.order.persistence.Orders;
import com.sapto.tiket.testcase.intermediate.order.repository.OrderRepository;

@Service("orderServiceRDBMS")
public class OrderServiceRDBMS implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Override
	public Page<Orders> findAllOrder(PageRequest pageableRequest) {
		Page<Orders> orders = orderRepository.findAll(pageableRequest);
		return orders;
	}

}
