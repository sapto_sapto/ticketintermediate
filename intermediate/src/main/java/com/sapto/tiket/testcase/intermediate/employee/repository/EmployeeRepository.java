package com.sapto.tiket.testcase.intermediate.employee.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.sapto.tiket.testcase.intermediate.employee.persistence.Employee;

public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Integer>{

}
