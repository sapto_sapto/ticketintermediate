package com.sapto.tiket.testcase.intermediate.customer.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.sapto.tiket.testcase.intermediate.persistence.BaseUser;
import com.sapto.tiket.testcase.intermediate.persistence.IBaseUser;

@Entity
public class Customer extends BaseUser implements IBaseUser, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer customerID;

	@Column(length = 50)
	private String companyName;
	@Column(length = 255)
	private String billingAddress;
	@Column(length = 50)
	private String stateOrProvince;
	@Column(length = 20)
	private String zIPCode;
	@Column(length = 75)
	private String email;
	@Column(length = 200)
	private String companyWebsite;
	@Column(length = 30)
	private String phoneNumber;
	@Column(length = 30)
	private String faxNumber;
	@Column(length = 255)
	private String shipAddress;
	@Column(length = 50)
	private String shipCity;
	@Column(length = 50)
	private String shipStateOrProvinc;
	@Column(length = 20)
	private String shipZlPCode;
	@Column(length = 30)
	private String shipPhoneNumber;

	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public Customer(Integer customerID, String companyName, String firstName, String lastName, String billingAddress,
			String stateOrProvince, String zIPCode, String email, String companyWebsite, String phoneNumber,
			String faxNumber, String shipAddress, String shipCity, String shipStateOrProvinc, String shipZlPCode,
			String shipPhoneNumber) {
		super();
		this.customerID = customerID;
		this.companyName = companyName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.billingAddress = billingAddress;
		this.stateOrProvince = stateOrProvince;
		this.zIPCode = zIPCode;
		this.email = email;
		this.companyWebsite = companyWebsite;
		this.phoneNumber = phoneNumber;
		this.faxNumber = faxNumber;
		this.shipAddress = shipAddress;
		this.shipCity = shipCity;
		this.shipStateOrProvinc = shipStateOrProvinc;
		this.shipZlPCode = shipZlPCode;
		this.shipPhoneNumber = shipPhoneNumber;
	}

	public Integer getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(length = 30)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(length = 50)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getStateOrProvince() {
		return stateOrProvince;
	}

	public void setStateOrProvince(String stateOrProvince) {
		this.stateOrProvince = stateOrProvince;
	}

	public String getzIPCode() {
		return zIPCode;
	}

	public void setzIPCode(String zIPCode) {
		this.zIPCode = zIPCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getShipAddress() {
		return shipAddress;
	}

	public void setShipAddress(String shipAddress) {
		this.shipAddress = shipAddress;
	}

	public String getShipCity() {
		return shipCity;
	}

	public void setShipCity(String shipCity) {
		this.shipCity = shipCity;
	}

	public String getShipStateOrProvinc() {
		return shipStateOrProvinc;
	}

	public void setShipStateOrProvinc(String shipStateOrProvinc) {
		this.shipStateOrProvinc = shipStateOrProvinc;
	}

	public String getShipZlPCode() {
		return shipZlPCode;
	}

	public void setShipZlPCode(String shipZlPCode) {
		this.shipZlPCode = shipZlPCode;
	}

	public String getShipPhoneNumber() {
		return shipPhoneNumber;
	}

	public void setShipPhoneNumber(String shipPhoneNumber) {
		this.shipPhoneNumber = shipPhoneNumber;
	}

}
