package com.sapto.tiket.testcase.intermediate.product.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer productID;

	@Column(length = 50)
	private String productName;

	@Column
	private Double unitPrice;

	@Column(length = 1)
	private String inStock;
	
	public Product() {
		// TODO Auto-generated constructor stub
	}
	
	public Product(Integer productID, String productName, Double unitPrice, String inStock) {
		this.productID = productID;
		this.productName=productName;
		this.unitPrice=unitPrice;
		this.inStock=inStock;
	}
	
	

	public Integer getProductID() {
		return productID;
	}

	public void setProductID(Integer productID) {
		this.productID = productID;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getInStock() {
		return inStock;
	}

	public void setInStock(String inStock) {
		this.inStock = inStock;
	}

}
