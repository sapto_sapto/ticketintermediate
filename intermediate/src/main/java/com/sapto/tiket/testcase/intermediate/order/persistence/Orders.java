package com.sapto.tiket.testcase.intermediate.order.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sapto.tiket.testcase.intermediate.customer.persistence.Customer;
import com.sapto.tiket.testcase.intermediate.employee.persistence.Employee;
import com.sapto.tiket.testcase.intermediate.product.persistence.Product;

@Entity
public class Orders implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Integer orderID;

	@Column
	private Date orderDate;

	@Column(length = 30)
	private String purchaseOrderNumber;

	@Column
	private Date ShipDate;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "shipping_method_shipping_methodid")
	private ShippingMethod shippingMethod;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "employee_employeeid")
	private Employee employee;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_customerid")
	private Customer customer;

	@Column
	private Integer freightCharge;
	@Column
	private Integer taxes;
	@Column(length = 1)
	private String paymentReceived;
	@Column(length = 150)
	private String comment;

	@OneToMany(mappedBy = "orders", fetch = FetchType.EAGER)
	@JsonIgnore
	private List<OrderDetail> orderDetails;
	
	@Transient
	private Double priceBeforeTaxAndShipment;
	
	@Transient
	private Double finalPrice;
	

	@Transient
	private List<ProductOrderInfo> productOrderInfos;

	public Orders() {
		// TODO Auto-generated constructor stub
	}

	public Orders(Integer orderID, Date orderDate, String purchaseOrderNumber, Date shipDate,
			ShippingMethod shippingMethod, Employee employee, Customer customer, Integer freightCharge, Integer taxes,
			String paymentReceived, String comment) {
		super();
		this.orderID = orderID;
		this.orderDate = orderDate;
		this.purchaseOrderNumber = purchaseOrderNumber;
		this.ShipDate = shipDate;
		this.shippingMethod = shippingMethod;
		this.employee = employee;
		this.customer = customer;
		this.freightCharge = freightCharge;
		this.taxes = taxes;
		this.paymentReceived = paymentReceived;
		this.comment = comment;
	}

	public Integer getOrderID() {
		return orderID;
	}

	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public Date getShipDate() {
		return ShipDate;
	}

	public void setShipDate(Date shipDate) {
		ShipDate = shipDate;
	}

	public Integer getFreightCharge() {
		return freightCharge;
	}

	public void setFreightCharge(Integer freightCharge) {
		this.freightCharge = freightCharge;
	}

	public Integer getTaxes() {
		return taxes;
	}

	public void setTaxes(Integer taxes) {
		this.taxes = taxes;
	}

	public String getPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(String paymentReceived) {
		this.paymentReceived = paymentReceived;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public ShippingMethod getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(ShippingMethod shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public List<ProductOrderInfo> getProductOrderInfos() {
		List<ProductOrderInfo> productOrderInfosCurrent = new ArrayList<>();
		for (OrderDetail orderDetail : orderDetails) {
			Product product = orderDetail.getProduct();

			ProductOrderInfo productOrderInfo = new ProductOrderInfo(product.getProductName(),
					orderDetail.getQuantity(), product.getUnitPrice(),
					product.getUnitPrice() * orderDetail.getQuantity(), orderDetail.getDiscount());
			productOrderInfosCurrent.add(productOrderInfo);

		}
		return productOrderInfosCurrent;
	}

	public void setProductOrderInfos(List<ProductOrderInfo> productOrderInfos) {
		this.productOrderInfos = productOrderInfos;
	}

	public Double getPriceBeforeTaxAndShipment() {
		List<ProductOrderInfo> productOrderInfos = getProductOrderInfos();
		Double priceBeforeTax = new Double(0.0);
		for (ProductOrderInfo productOrderInfo : productOrderInfos) {
			priceBeforeTax += productOrderInfo.getTotalPrice();
		}
		
		
		
		
		return priceBeforeTax;
	}

	public void setPriceBeforeTaxAndShipment(Double priceBeforeTaxAndShipment) {
		this.priceBeforeTaxAndShipment = priceBeforeTaxAndShipment;
	}

	public Double getFinalPrice() {
		Double totalPrice = getPriceBeforeTaxAndShipment();
		Integer deliveryFee = getFreightCharge();
		if(deliveryFee==null) {
			deliveryFee=0;
		}
		totalPrice = totalPrice+deliveryFee+getTaxes();
		return totalPrice;
	}

	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}

	// Daftar produk yang dipesan beserta kuantitas, harga satuan, diskon dan harga subtotal per item baris: nama,qty, harga, diskon, subtotal
	// Detail item pembayaran (subtotal pemesanan, pajak, biaya pengiriman dan total  biaya keseluruhan): totalharga, biaya kirim, pajak, total

	
}
