package com.sapto.tiket.testcase.intermediate.order.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.sapto.tiket.testcase.intermediate.order.persistence.Orders;

public interface OrderRepository extends PagingAndSortingRepository<Orders, Integer> {
	

}
