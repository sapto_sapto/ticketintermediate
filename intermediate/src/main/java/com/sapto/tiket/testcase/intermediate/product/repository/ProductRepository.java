package com.sapto.tiket.testcase.intermediate.product.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.sapto.tiket.testcase.intermediate.product.persistence.Product;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {

}
