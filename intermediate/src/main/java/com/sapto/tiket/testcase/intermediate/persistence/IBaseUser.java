package com.sapto.tiket.testcase.intermediate.persistence;

public interface IBaseUser {
	
	public String getLastName();

	public void setLastName(String lastName);
	
	public String getFirstName();

	public void setFirstName(String firstName);

}
