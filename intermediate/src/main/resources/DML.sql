--query customer location at Irvince
SELECT * FROM ecommerce.customer where ship_city = 'Irvine';

--Daftar semua pelanggan yang pesanannya ditangani karyawan bernama Adam Barr
SELECT cst.* FROM ecommerce.orders as ord , ecommerce.customer as cst, ecommerce.employee as emp where emp.first_name = 'Adam' and emp.last_name = 'Barr' and ord.employee_employeeid = emp.employeeid and ord.customer_customerid = cst.customerid  group by cst.customerid;

--Daftar produk yang dipesan oleh pelanggan Contoso, Ltd
select products.*
 from (
	select product.*, orderDet.orders_orderid as orderID from ecommerce.order_detail as orderDet, ecommerce.product as product
	where orderDet.product_productid = product.productid) as products, ecommerce.customer, ecommerce.orders
where products.orderID = orders.orderid
and orders.customer_customerid = customer.customerid
and customer.company_name = 'Contoso, Ltd';


--Daftar transaksi pemesanan yang dikirimkan melalui UPS Ground
SELECT ord.* FROM ecommerce.orders as ord 
	left join ecommerce.shipping_method as shpmtd
    on ord.shipping_method_shipping_methodid = shpmtd.shipping_methodid
    where shpmtd.shipping_method_name = 'UPS Ground';

--Daftar biaya total pemesanan (termasuk pajak dan biaya pengiriman) setiap transaksi diurut berdasarkan tanggal transaksi
SELECT ((ordDet.quantity * ordDet.unit_price) +  ifnull(ord.freight_charge, 0) + ord.taxes ) as totalCost,
	ordDet.quantity as qty, ordDet.unit_price as price, ord.freight_charge as feeDelivery, ord.taxes as tax, ord.order_date  as trxDate
	FROM ecommerce.orders as ord  
	inner join  ecommerce.order_detail as ordDet
    on ord.orderid = ordDet.orders_orderid
    order by ord.order_date desc;