CREATE DATABASE  IF NOT EXISTS `ecommerce` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ecommerce`;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `customerid` int(11) NOT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `company_name` varchar(50) DEFAULT NULL,
  `company_website` varchar(200) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `fax_number` varchar(30) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  `ship_address` varchar(255) DEFAULT NULL,
  `ship_city` varchar(50) DEFAULT NULL,
  `ship_phone_number` varchar(30) DEFAULT NULL,
  `ship_state_or_provinc` varchar(50) DEFAULT NULL,
  `ship_zlpcode` varchar(20) DEFAULT NULL,
  `state_or_province` varchar(50) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`customerid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `employeeid` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `work_phone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`employeeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail` (
  `order_detailid` int(11) NOT NULL,
  `discount` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  `orders_orderid` int(11) DEFAULT NULL,
  `product_productid` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_detailid`),
  KEY `FKigseyi9f1ckppdao7ta23w6s9` (`orders_orderid`),
  KEY `FKnyn10bjsd29ol9pfl6h2t85km` (`product_productid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `orderid` int(11) NOT NULL,
  `ship_date` datetime DEFAULT NULL,
  `comment` varchar(150) DEFAULT NULL,
  `freight_charge` int(11) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `payment_received` varchar(1) DEFAULT NULL,
  `purchase_order_number` varchar(30) DEFAULT NULL,
  `taxes` int(11) DEFAULT NULL,
  `customer_customerid` int(11) DEFAULT NULL,
  `employee_employeeid` int(11) DEFAULT NULL,
  `shipping_method_shipping_methodid` int(11) DEFAULT NULL,
  PRIMARY KEY (`orderid`),
  KEY `FKp59f1fqj0889eepfbaaygp3hs` (`customer_customerid`),
  KEY `FKsy72f03w6i8qmvwfi9h1raua1` (`employee_employeeid`),
  KEY `FKxjbeh1ndqmxmriackqw89knt` (`shipping_method_shipping_methodid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `productid` int(11) NOT NULL,
  `in_stock` varchar(1) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `shipping_method`
--

DROP TABLE IF EXISTS `shipping_method`;
CREATE TABLE `shipping_method` (
  `shipping_methodid` int(11) NOT NULL,
  `shipping_method_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`shipping_methodid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
